#include <iostream>
#include <string>
#include <sstream>
using namespace std;

bool numberwang(int number_attempt)
{
	bool wang = false;
	
	if(number_attempt%3 == 0 || number_attempt % 5 == 0)
	{
		wang = true;
	}
	
	else
	{
		wang = false;
	}
	
	return wang;
}

int main()
{
	bool game_on = true;
	bool no_round_winner = true;
	bool current_player = false; // true is player 1 false is player 2
	bool winner = true; //if true player 1 won if false player 2
	int round = 0;
	float high_number = 0;
	float low_number = 0;
	string player_1_name;
	string player_2_name;
	string number_of_rounds;
	string attempt;
	
	cout << "Hello, and welcome to Numberwang, the maths quiz that simply everyone is talking about!" << endl;
	cout << "What is player 1's name?" << endl;
	cin >> player_1_name;
	cout << "What is player 2's name?" << endl;
	cin >> player_2_name;
	cout << "How many rounds?" << endl;
	cin >> number_of_rounds;
	
	stringstream my_converter(number_of_rounds);
	int rounds;
	if(!(my_converter >> rounds)) 
	{
		rounds = 0;
	}
	cout << "Well, if you're ready, let's play Numberwang!" << endl;
	
	while(game_on)
	{
		if(round == rounds)
		{
			game_on = false;
			break;
		}
		
		if(round%2 == 0)
		{
			cout << "Round, " << round + 1 << " " << player_1_name << " to play first." << endl;
			current_player = true;
		}
		else 
		{
			cout << "Round, " << round + 1 << " " << player_2_name << " to play first." << endl;
			current_player = false;
		}
		
		while(no_round_winner)
		{
			if(current_player)
			{
				cout << player_1_name << ": ";
			}
			else
			{
				cout << player_2_name << ": ";
			}
			
			cin >> attempt;
			
			stringstream my_converter(attempt);
			int attempt_int;
			
			if(!(my_converter >> attempt_int)) 
			{
				attempt_int = 0;
			}
			
			if(numberwang(attempt_int))
			{
				if(high_number < attempt_int)
				{
					high_number = attempt_int;
					if(current_player)
					{
						winner = true;
					}
					else
					{
						winner = false;
					}
				}
				no_round_winner = false ;
			}
			current_player = !current_player;
		}
		
		cout << "That's Numberwang!" << endl;
		round++;
		no_round_winner = true;
	}
	
	low_number = high_number - 10;
	
	if(winner)
	{
		cout << "Final scores: " << player_1_name << " pulls ahead with " << high_number << ", and " << player_2_name << " finishes with " << low_number << "." << endl;
	}
	
	else
	{
		cout << "Final scores: " << player_2_name << " pulls ahead with " << high_number << ", and " << player_1_name << " finishes with " << low_number << "." << endl;
	}
}